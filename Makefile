compose-build:
	docker-compose -f docker/compose.yml --project-directory . build

compose-up: compose-build
	docker-compose -f docker/compose.yml --project-directory . up -d

compose-start: compose-build
	docker-compose -f docker/compose.yml --project-directory . start

compose-stop:
	docker-compose -f docker/compose.yml --project-directory . stop 

compose-rm: compose-stop
	docker-compose -f docker/compose.yml --project-directory . rm 

compose-logs:
	docker-compose -f docker/compose.yml --project-directory . logs 

local-docker-image:
	docker build -t registry.gitlab.ifma.edu.br/ndsis/evote/helios-server/develop:latest .

local-k8s: dev-docker-image
	eval $(minikube docker-env)
	kubectl create namespace helios || true
	helm dependency build ci/helios-server || helm dependency update ci/helios-server
	helm upgrade -n helios -i helios-server --set image.repository=registry.gitlab.ifma.edu.br/ndsis/evote/helios-server/develop --set image.tag=latest ci/helios-server
	kubectl rollout status deployment -n helios helios-server
