# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2020-06-08 13:19
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('heliosinstitution', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='institutionuserprofile',
            name='django_user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
