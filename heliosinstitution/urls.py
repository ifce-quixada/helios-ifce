"""
Institution related urls
Shirlei Chaves (shirlei@gmail.com)

"""
from django.conf.urls import url
from views import *


urlpatterns = [
	url(r'^$', home),
	url(r'^dashboard$', dashboard),
	url(r'^stats$', stats),
	url(r'^manage_users$', manage_users),
	url(r'^users$', users),
	url(r'^admin-actions$', admin_actions),
	url(r'^(?P<institution_pk>\d+)/elections/(?P<type>\w+)/(?P<year>\d{4})?$', elections_by_type_year),
	url(r'^(?P<institution_pk>\d+)/details$', institution_details),
	url(r'^elections/(?P<year>\d{4})?$', elections_by_year),
	url(r'^elections/summary/(?P<year>\d{4})?$', elections_summary),
	url(r'^(?P<role>[\w\ ]+)/delegate_user$', delegate_user),
	url(r'^(?P<user_pk>\d+)/revoke_user$', revoke_user),
	url(r'^(?P<user_pk>\d+)/user_metadata$', user_metadata),
	url(r'^(?P<user_pk>\d+)/add_expires_at$', add_expires_at),
	url(r'^users/(?P<user_pk>\d+)/elections/administered$', elections_administered),
]
