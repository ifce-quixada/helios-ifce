// performance-test.js

import { sleep } from "k6";
import http from "k6/http";

export let options = {
  duration: "30s",
    vus: 500,
    thresholds: {
    // 90% of requests must finish within 400ms, 95% within 800, and 99.9% within 2s.
    'http_req_duration': ['p(90) < 400', 'p(95) < 800', 'p(99.9) < 2000'],
  }
};

export default function() {
  http.get("https://eleicao.ifma.edu.br");
  sleep(5);
}
