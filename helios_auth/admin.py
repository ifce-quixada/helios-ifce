from django.contrib import admin
from helios.models import User


class UserAdmin(admin.ModelAdmin):	
    exclude = ('info', 'token')
    list_display = ('name', 'user_id')
    # IFMA
    search_fields = ('name', )
    list_filter = ('user_type', )
    # IFMA Fim


admin.site.register(User, UserAdmin)
