# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2020-06-24 12:37
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('helios_auth', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL("ALTER TABLE auth_user ALTER COLUMN last_name TYPE character varying(100);")
    ]
