#!/bin/sh

# Parametros
# 1 - Quantidade de Contas
# 2 - Inicio do email - ex.: helios-mail_ -> resultado helios-mail_01...
# 3 - Dominio do email - ex.: empresa.com.br
# 4 - Senha do usuário de email

echo "Gerando variáveis para $1 senders"

cnt=0

echo MAILER_NUM_ACCOUNTS=$1
echo MAILER_DAILY_SENDING_LIMIT_PER_ACCOUNT=9998

for cnt in $(seq 1 $1); do
    c=$(printf "%02d" $cnt)
    echo EMAIL_HOST_$cnt='smtp-relay.gmail.com'
    echo EMAIL_PORT_$cnt='587'
    echo EMAIL_HOST_USER_$cnt="$2$c@$3"
    echo EMAIL_HOST_PASSWORD_$cnt=\'"$4"\'
    echo EMAIL_USE_TLS_$cnt=True
done
