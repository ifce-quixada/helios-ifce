#!/bin/sh

# populate directory
rsync -avz /orig/* /app/.

source /env/bin/activate
python manage.py compilemessages
# python manage.py makemigration
python manage.py collectstatic --no-input
python manage.py migrate
# echo "from helios_auth.models import User; User.objects.create(user_type='google',user_id='ben@adida.net', info={'name':'Ben Adida'})" | python manage.py shell

# python manage.py runserver 0.0.0.0:8000
exec "$@"

#gunicorn wsgi -b 0.0.0.0:8000
